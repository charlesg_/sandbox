#!/usr/local/bin/python

# This script downloads CME data from SlickCharts.
# They do not update constantly so I run everyday and try to update
# the past 7 days. Keeps my repository up-to-date.

import getopt, sys, os
from datetime import date,timedelta
from os.path import basename
from os import system


# What to update
update = "scdata" 
target = "/home/charlesg/markets/CME"
srcSite = "http://www.slickcharts.com/"
lckDir = target + "/lock"
lck = lckDir + "/" + basename(sys.argv[0])

def main():

    today = date.today()
    d = today

    for x in range(1,7):
        d = (d - timedelta(1))
        #wget http://www.slickcharts.com/scdata/gx120222.zip
        myTarget = target+"/"+update+"/"+str(d.year)+"/"+str(d.year)+str(d.month).zfill(2)+"/"
        os.system("/usr/bin/wget -cr -nH -nd -P {0} --retry-connrefused {1}//{2}/gx{3:02d}{4:02d}{5:02d}.zip".format(myTarget,srcSite,update,d.year-2000,d.month,d.day))
    

def usage():
    print "This script downloads historical data from SlickCharts."

if __name__ == "__main__":
    main()

