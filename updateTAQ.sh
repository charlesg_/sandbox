#!/bin/bash

# Keeps up to data set of file downloaded from NYSE data up to date.
# Because of the inconsistencies at NYSE, I run this everything 15
# minutes throughout the day:
# 
#*/15 * * * * /home/hudson/runtime/hudson_run "firm.hcpl4.taqdata.cron" /hhc/data/DailyTAQ/update > /dev/null 2>&1
#*/15 0-9 1 * * /home/hudson/runtime/hudson_run "firm.hcpl4.taqdata.cron" /hhc/data/DailyTAQ/update_on1st > /dev/null 2>&1
#
# This script does not handle 


# What to update
UPDATE="EQY_US_ALL_TRADE EQY_US_ALL_BBO EQY_US_ALL_ADMIN EQY_US_ALL_REF_MASTER_PD EQY_US_ALL_REF_MASTER" 
TARGET="/home/charlesg/markets/DailyTAQ"
FROM="ftp://ftp2.nyxdata.com"
LOCKDIR=${TARGET}/lock
LOCK=${LOCKDIR}/$(basename $0)
BATCHMON="batchon"
BATCHMONHOST="serverName"
BATCHMONUSER="batchmon"
BATCHMONPASS="########"

# We update the current month
# On the 1st of January, we run one more December a year ago update.
# On the 1st of the month, we run one more a month ago update.
DAY=$(date +%d)
MONTH=$(date +%m)
YEAR=$(date +%Y)
if [ $MONTH -eq 1 -a $DAY -eq 1 ]; then
    YEAR=$(date '+%Y' --date '1 year ago')
fi
if [ $DAY -eq 1 ]; then
    MONTH=$(date '+%m' --date '1 month ago')
fi

# BatchMon marker
/usr/bin/mysql -h ${BATCHMONHOST} -u ${BATCHMONUSER} -p${BATCHMONPASS} ${BATCHMON} << EOF
INSERT 
	INTO batch_runs 
	(jobName, executedOn) values ("DailyTAQUpdate",NOW())
	ON DUPLICATE KEY UPDATE executedOn=NOW();
EOF

# Locking process
if mkdir ${LOCK}; then
	echo "Locking Succeeded."
	echo $$ > ${LOCK}/pid
else
	echo "Locking failed - exit"
	echo "update already running ($(cat ${LOCK}/pid))"
	exit 1
fi

for dir in $UPDATE
do
	cd ${TARGET}
	wget \
		-cr -nH --retry-connrefused \
		--ftp-user=####### --ftp-password=####### \
		${FROM}//${dir}/${dir}_${YEAR}/${dir}_${YEAR}${MONTH}/
done

# Cleanup
rm ${LOCK}/pid
rmdir ${LOCK}

# On the 1st, from midnight to 9am we run update on prior month
MONTH=$(date '+%m' --date '1 month ago')

